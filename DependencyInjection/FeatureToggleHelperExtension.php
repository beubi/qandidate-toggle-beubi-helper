<?php

namespace FeatureToggleHelperBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class FeatureToggleHelperExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $activatorDef = new Definition('FeatureToggleHelperBundle\Command\ActivateTogglesCommand');
        $activatorDef->addArgument(new Reference($config['redis_client']));
        $activatorDef->addArgument($config['redis_namespace']);
        $activatorDef->addArgument(new Reference($config['toggles']));
        $activatorDef->addTag('console.command');
        $container->setDefinition('feature_toggle_helper.activatecogglescommand', $activatorDef);

        $bootstrapDef = new Definition('FeatureToggleHelperBundle\Command\BootstrapTogglesCommand');
        $bootstrapDef->addArgument(new Reference($config['redis_client']));
        $bootstrapDef->addArgument($config['redis_namespace']);
        $bootstrapDef->addArgument(new Reference($config['toggles']));
        $bootstrapDef->addTag('console.command');
        $container->setDefinition('feature_toggle_helper.bootstraptogglescommand', $bootstrapDef);
        
        $deactivateDef = new Definition('FeatureToggleHelperBundle\Command\DeactivateTogglesCommand');
        $deactivateDef->addArgument(new Reference($config['redis_client']));
        $deactivateDef->addArgument($config['redis_namespace']);
        $deactivateDef->addArgument(new Reference($config['toggles']));
        $deactivateDef->addTag('console.command');
        $container->setDefinition('feature_toggle_helper.deactivatetogglescommand', $deactivateDef);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}

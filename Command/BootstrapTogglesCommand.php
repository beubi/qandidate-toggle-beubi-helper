<?php

namespace FeatureToggleHelperBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FeatureToggleHelperBundle\Toggle\TogglesInterface;

/**
 * Command that insert some values into redis about toggle features from Qandidate
 */
class BootstrapTogglesCommand extends Command
{
    /** @var \Predis\Client  */
    private $predis;
    private $namespace;
    private $toggles;

    protected function configure()
    {
        $this
            ->setName('featuretogglehelper:bootstrap-toggles')
            ->setDescription('This command will add toggles to be used by qandidate toggling');
    }

    /**
     * @param \Predis\Client   $predis
     * @param string           $namespace
     * @param TogglesInterface $toggles
     */
    public function __construct(\Predis\Client $predis, $namespace, TogglesInterface $toggles)
    {
        $this->predis = $predis;
        $this->namespace = $namespace;
        $this->toggles = $toggles->getToggles();
        parent::__construct();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $toggleCollection = new \Qandidate\Toggle\ToggleCollection\PredisCollection($this->namespace, $this->predis);

        // Removing toggles that aren't in Toggles::TOGGLES
        foreach ($this->predis->keys($this->namespace.'__TOGGLE__*') as $key) {
            $matches = [];
            preg_match('/^.*:(.*)/', $key, $matches);
            $unserializedToggle = unserialize($this->predis->get($matches[1]));
            $toggleName = $unserializedToggle->getName();

            if (!in_array($toggleName, $this->toggles)) {
                $output->writeln('Removing '.$toggleName);
                $toggleCollection->remove($toggleName);
            }
        }

        // adding toggles that weren't already added
        foreach ($this->toggles as $featureFlag) {
            if (null === $toggleCollection->get($featureFlag)) {
                $output->writeln('Adding '.$featureFlag);
                $toggle = new \Qandidate\Toggle\Toggle($featureFlag, []);
                $toggle->deactivate();
                $toggleCollection->set($featureFlag, $toggle);
            }
        }

        $output->writeln('Done');
    }
}

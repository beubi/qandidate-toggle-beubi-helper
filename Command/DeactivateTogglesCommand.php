<?php

namespace FeatureToggleHelperBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FeatureToggleHelperBundle\Toggle\TogglesInterface;

/**
 * Command that activates feature toggles
 * Used mostly during tests
 */
class DeactivateTogglesCommand extends Command
{
    /** @var \Predis\Client  */
    private $predis;
    private $namespace;
    private $toggles;

    protected function configure()
    {
        $this
            ->setName('featuretogglehelper:deactivate-toggles')
            ->setDescription('This command will deactivate toggles to be used by qandidate toggling')
            ->addArgument('toggles', \Symfony\Component\Console\Input\InputArgument::IS_ARRAY, 'name/s of the toggle/s')
            ->addOption('all', null, \Symfony\Component\Console\Input\InputOption::VALUE_NONE, 'whether to deactivate all toggles or not. When used, other arguments will be ignored');
    }

    /**
     * @param \Predis\Client   $predis
     * @param string           $namespace
     * @param TogglesInterface $toggles
     */
    public function __construct(\Predis\Client $predis, $namespace, TogglesInterface $toggles)
    {
        $this->predis = $predis;
        $this->namespace = $namespace;
        $this->toggles = $toggles->getToggles();
        parent::__construct();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $toggleCollection = new \Qandidate\Toggle\ToggleCollection\PredisCollection('toggle', $this->predis);
        if ($input->getOption('all')) {
            foreach ($this->toggles as $toggleName) {
                $output->writeln('Deactivating '.$toggleName);
                $toggle = new \Qandidate\Toggle\Toggle($toggleName, []);
                $toggle->deactivate();
                $toggleCollection->set($toggleName, $toggle);
            }

            return null;
        }
        $toggles = $input->getArgument('toggles');
        if (!count($toggles)) {
            $output->writeln('No toggles given so nothing to do.');

            return null;
        }
        foreach ($toggles as $toggleName) {
            if (!in_array($toggleName, $this->toggles, true)) {
                $output->writeln('The toggle "'.$toggleName.'" does not exists. Not deactivating...');
                continue;
            }
            $output->writeln('Deactivating '.$toggleName);
            $toggle = new \Qandidate\Toggle\Toggle($toggleName, []);
            $toggle->deactivate();
            $toggleCollection->set($toggleName, $toggle);
        }

        $output->writeln('Done');
    }
}

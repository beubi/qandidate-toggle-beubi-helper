<?php

namespace FeatureToggleHelperBundle\Toggle;

use Composer\Script\Event;

/**
 * Script Handler to be used by composer to execute bootstrapping toggles command after a composer event
 */
class ComposerScriptHandler extends \Sensio\Bundle\DistributionBundle\Composer\ScriptHandler
{
    /**
     * @param CommandEvent $event
     *
     * @return null
     */
    public static function bootstrapToggles(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'bootstrap toggles');

        if (null === $consoleDir) {
            return;
        }

        static::executeCommand($event, $consoleDir, 'featuretogglehelper:bootstrap-toggles', $options['process-timeout']);
    }
}

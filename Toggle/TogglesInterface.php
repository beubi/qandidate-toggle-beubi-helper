<?php

namespace FeatureToggleHelperBundle\Toggle;

/**
 *
 */
interface TogglesInterface
{
    public function getToggles();
}

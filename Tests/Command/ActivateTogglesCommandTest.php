<?php

namespace FeatureToggleHelperBundle\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ActivateTogglesCommandTest extends KernelTestCase
{
    private $namespace = 'toggle';

    private function removeAllToggles()
    {
        $toggleCollection = new \Qandidate\Toggle\ToggleCollection\PredisCollection($this->namespace, $this->predis);

        foreach ($this->predis->keys($this->namespace.'__TOGGLE__*') as $key) {
            $matches = [];
            preg_match('/^.*:(.*)/', $key, $matches);
            $unserializedToggle = unserialize($this->predis->get($matches[1]));
            $toggleName = $unserializedToggle->getName();
            $toggleCollection->remove($toggleName);
        }
    }

    public function testIndex()
    {
        $kernel = static::bootKernel();
        $this->predis = $kernel->getContainer()->get('snc_redis.toggle');
        $this->removeAllToggles();
        $this->assertCount(0, $this->predis->keys($this->namespace.'__TOGGLE__*'));
        $command = new \FeatureToggleHelperBundle\Command\ActivateTogglesCommand(
            $this->predis,
            'toggles',
            new class implements \FeatureToggleHelperBundle\Toggle\TogglesInterface {
                /**
                 * @return array
                 */
                public function getToggles()
                {
                    return [];
                }
            }
        );
        $commandTester = new \Symfony\Component\Console\Tester\CommandTester($command);
        $commandTester->execute([]);
        $output = $commandTester->getDisplay();
        print_r("\n");
        print_r($output);
        print_r("\n");
        $this->assertCount(0, $this->predis->keys($this->namespace.'__TOGGLE__*'));
    }
}
